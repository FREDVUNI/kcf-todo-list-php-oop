# TODO OOP PHP TODO APP

## Requirements

- OOP PHP
- MySQL
- FontAwesome
- Google Fonts
- CSS

## Installation

1. Create a new MySQL database and import the `tasks_todo.sql` file included in this repository (database) to create the required table.
2. Open `database/database.php` and update the `$host`, `$username`, `$password`, and `$database` variables with your database connection details.
3. Open `index.php` in a web browser to view the application. `localhost/DIR_NAME/index.php`

## Usage

The application has a simple interface with an input field to add new tasks and a list of tasks.

### Adding a task

To add a new task, simply type in the task description in the input field and press the "Add" button. The task will be added to the list and also saved to the database.

### Editing a task

To edit a task, click on the "Edit" icon next to the task. This will convert the task label into an input field where you can edit the task description.

### Deleting a task

To delete a task, click on the "Delete" icon next to the task. This will remove the task from the list and also delete it from the database.

## Implementation details

The application uses PHP scripts that interact with the MySQL database. The PHP scripts return JSON responses that are parsed by the JavaScript code to update the DOM.

### database.php

This file defines a `Database` class that handles the database connection and provides methods to perform CRUD operations on the `tasks` table.

### tasks_todo.sql

This file contains the SQL code to create the `tasks` table.
