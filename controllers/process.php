<?php
require_once(__DIR__ . '/../database/database.php');

class DatabaseOperations {
    private $conn;

    public function __construct($conn) {
        $this->conn = $conn;
    }

    public function addTask($task) {
        if(strlen($task) > 25){
            header("Location: ../index.php?error=Todo item should not be more than 25 words.");
            exit();
        }

        $stmt = $this->conn->prepare("INSERT INTO tasks (task) VALUES (?)");
        $stmt->bind_param("s", $task);
        $stmt->execute();
    }

    public function getTasks() {
        $sql = "SELECT * FROM tasks ORDER BY id DESC";
        $result = $this->conn->query($sql);
        $tasks = array();

        if ($result->num_rows > 0):
            while($row = $result->fetch_assoc()):
                $tasks[] = $row;
            endwhile;
        endif;

        return $tasks;
    }

    public function getTaskById($id) {
        $stmt = $this->conn->prepare("SELECT * FROM tasks WHERE id = ?");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $task = $result->fetch_assoc();
        $stmt->close();
        return $task;
    }    

    public function deleteTask($id) {
        $sql = "DELETE FROM tasks WHERE id=$id";
        $result = $this->conn->query($sql);

        if (!$result) {
            header("Location:../index.php?error=There was an error deleting task.");
            exit();
        }
    }

    public function updateTask($id, $task) {
        if (!$id || !$task) {
            header("Location:../edit.php?error=Invalid input or id.");
            exit();
        }

        $sql = "UPDATE tasks SET task='$task' WHERE id=$id";
        $result = $this->conn->query($sql);

        if (!$result) {
            header("Location:../edit.php?error=There was an error updating task.");
            exit();
        }
    }
}

?>