<?php
    require_once("../database/database.php");
    require_once("../controllers/process.php");

    $database = new DatabaseOperations($conn);

    if (isset($_POST['todoInput']) && isset($_POST['id'])) {
        $database->updateTask($_POST['id'],$_POST['todoInput']);
    }

    header("Location:../index.php?success=Task has been updated.");
    exit();
?>
