<?php
    require_once("../database/database.php");
    require_once("../controllers/process.php");

    $database = new DatabaseOperations($conn);

    if (isset($_POST['todoInput'])) {
        $database->addTask($_POST['todoInput']);
    }

    header("Location:../index.php?success=Task has been added.");
    exit();
?>
