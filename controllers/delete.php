<?php
    require_once(__DIR__ . '/../database/database.php');
    require_once(__DIR__ . '/../controllers/process.php');

    $database = new DatabaseOperations($conn);

    $id = isset($_GET["id"]) ? $_GET["id"] : null;

    if (!$id) {
        header("Location:../index.php?error=The task was not found.");
        exit();
    }

    $result = $database->deleteTask($id);

    if ($result === false) {
        header("Location:../index.php?error=The task was not found.");
        exit();
    } else {
        header("Location:../index.php?success=Task has been deleted.");
        exit();
    }
?>
