<ul id="todoList" class="list-group">
    <?php
        require_once("./database/database.php");
        require_once("./controllers/process.php");
        
        $database = new DatabaseOperations($conn);
        $tasks = $database->getTasks();

        if (count($tasks) > 0):
            foreach($tasks as $row):
                echo '<li class="list-group-item">';
                echo '<div class="form-check">';
                echo '<input type="checkbox" class="form-check" />';
                echo '<label class="check-label" for="check">'.htmlspecialchars($row['task']).'</label>';
                echo '</div>';
                echo '<div class="date">'.date('M d h:i:s a', strtotime($row['createdAt'])).'</div>';
                echo '<div class="actions">';
                echo '<a href="./edit.php?id='.$row['id'].'" class="edit"><i class="fas fa-pencil-alt"></i></a>';
                echo '<a href="./controllers/delete.php?id='.$row['id'].'" class="edit"><i class="fas fa-trash"></i></a>';
                echo '</div>';
                echo '</li>';
            endforeach;
        else:
            echo '<li class="list-group-item">No tasks found.</li>';
        endif;
    ?>
</ul>
