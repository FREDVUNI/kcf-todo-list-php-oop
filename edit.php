
<?php
require_once("./database/database.php");
require_once("./controllers/process.php");
    
$database = new DatabaseOperations($conn);

$id = isset($_GET["id"]) ? $_GET["id"] : null;

if (!$id) {
    header("Location:./index.php?error=The task was not found.");
    exit();
}

$task = $database->getTaskById($id);

if (!$task) {
    header("Location:./index.php?error=The task was not found.");
    exit();
}
?>

<!DOCTYPE html>
<html>
  <head>
    <title>PHP Todo list</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="./assets/style.css" />
    <link
      href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;600&display=swap"
      rel="stylesheet"
    />
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
    />
  </head>
  <body>
    <main>
      <div class="container">
        <div class="header">
        <h1>Edit <?php echo $task['task'];?></h1>
          <p class="success">
            <?php echo isset($_GET['success']) ? $_GET['success'] : ''; ?>
          </p>
          <p class="error"><?php echo isset($_GET['error']) ? $_GET['error'] : ''; ?></p>
          <form id="todoForm" method="POST" action="./controllers/update.php">
            <div class="form-row">
              <div class="input-container">
                <input type="hidden" name="id" value="<?php echo $id;?>">
                <input type="text" name="todoInput" id="todoInput" placeholder="Edit task..." value="<?php echo $task['task'];?>"/>
              </div>
              <div class="button-container">
                <span class="icon-container"
                  ><i class="fas fa-calendar fa-lg"></i
                ></span>
                <button type="submit">update</button>
              </div>
            </div>
          </form>
        </div>
        <div class="filter-sort-container">
          <div class="filter-container">
            <label for="filter">Filter:</label>
            <select id="filter">
              <option value="all">All</option>
              <option value="today">Today</option>
              <option value="tomorrow">Tomorrow</option>
            </select>
          </div>
          <div class="sort-container">
            <label for="sort">Sort:</label>
            <select id="sort">
              <option value="date-asc">Date (ascending)</option>
              <option value="date-desc">Date (descending)</option>
            </select>
          </div>
        </div>
        <?php include_once("./controllers/fetch.php");?>

        <footer>
          <p>
            copyright Todos &copy;
            <script>
              document.write(new Date().getFullYear());
            </script>
          </p>
        </footer>
      </div>
    </main>
    <script src="script.js"></script>
  </body>
</html>
